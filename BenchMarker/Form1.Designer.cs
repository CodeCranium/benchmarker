﻿namespace BenchMarker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_PathToApplication = new System.Windows.Forms.Label();
            this.txtbx_PathToApplication = new System.Windows.Forms.TextBox();
            this.btn_TestIt = new System.Windows.Forms.Button();
            this.dropdown_NumberOfTimesToProcess = new System.Windows.Forms.ComboBox();
            this.lbl_NumberOfTimesToProcess = new System.Windows.Forms.Label();
            this.lbl_TimeToProcess = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_PathToApplication
            // 
            this.lbl_PathToApplication.AutoSize = true;
            this.lbl_PathToApplication.Location = new System.Drawing.Point(12, 9);
            this.lbl_PathToApplication.Name = "lbl_PathToApplication";
            this.lbl_PathToApplication.Size = new System.Drawing.Size(96, 13);
            this.lbl_PathToApplication.TabIndex = 0;
            this.lbl_PathToApplication.Text = "Path to Application";
            // 
            // txtbx_PathToApplication
            // 
            this.txtbx_PathToApplication.Location = new System.Drawing.Point(114, 6);
            this.txtbx_PathToApplication.Name = "txtbx_PathToApplication";
            this.txtbx_PathToApplication.Size = new System.Drawing.Size(295, 20);
            this.txtbx_PathToApplication.TabIndex = 1;
            // 
            // btn_TestIt
            // 
            this.btn_TestIt.Location = new System.Drawing.Point(135, 71);
            this.btn_TestIt.Name = "btn_TestIt";
            this.btn_TestIt.Size = new System.Drawing.Size(140, 23);
            this.btn_TestIt.TabIndex = 2;
            this.btn_TestIt.Text = "Test It";
            this.btn_TestIt.UseVisualStyleBackColor = true;
            this.btn_TestIt.Click += new System.EventHandler(this.btn_TestIt_Click);
            // 
            // dropdown_NumberOfTimesToProcess
            // 
            this.dropdown_NumberOfTimesToProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdown_NumberOfTimesToProcess.FormattingEnabled = true;
            this.dropdown_NumberOfTimesToProcess.Location = new System.Drawing.Point(158, 32);
            this.dropdown_NumberOfTimesToProcess.Name = "dropdown_NumberOfTimesToProcess";
            this.dropdown_NumberOfTimesToProcess.Size = new System.Drawing.Size(251, 21);
            this.dropdown_NumberOfTimesToProcess.TabIndex = 3;
            // 
            // lbl_NumberOfTimesToProcess
            // 
            this.lbl_NumberOfTimesToProcess.AutoSize = true;
            this.lbl_NumberOfTimesToProcess.Location = new System.Drawing.Point(12, 35);
            this.lbl_NumberOfTimesToProcess.Name = "lbl_NumberOfTimesToProcess";
            this.lbl_NumberOfTimesToProcess.Size = new System.Drawing.Size(140, 13);
            this.lbl_NumberOfTimesToProcess.TabIndex = 4;
            this.lbl_NumberOfTimesToProcess.Text = "Number of Times to Process";
            // 
            // lbl_TimeToProcess
            // 
            this.lbl_TimeToProcess.AutoSize = true;
            this.lbl_TimeToProcess.Location = new System.Drawing.Point(23, 125);
            this.lbl_TimeToProcess.Name = "lbl_TimeToProcess";
            this.lbl_TimeToProcess.Size = new System.Drawing.Size(0, 13);
            this.lbl_TimeToProcess.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 180);
            this.Controls.Add(this.lbl_TimeToProcess);
            this.Controls.Add(this.lbl_NumberOfTimesToProcess);
            this.Controls.Add(this.dropdown_NumberOfTimesToProcess);
            this.Controls.Add(this.btn_TestIt);
            this.Controls.Add(this.txtbx_PathToApplication);
            this.Controls.Add(this.lbl_PathToApplication);
            this.Name = "Form1";
            this.Text = "Bench Marker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_PathToApplication;
        private System.Windows.Forms.TextBox txtbx_PathToApplication;
        private System.Windows.Forms.Button btn_TestIt;
        private System.Windows.Forms.ComboBox dropdown_NumberOfTimesToProcess;
        private System.Windows.Forms.Label lbl_NumberOfTimesToProcess;
        private System.Windows.Forms.Label lbl_TimeToProcess;
    }
}

