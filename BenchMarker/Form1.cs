﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BenchMarker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            AddNumbersForNumberOfTimesToProcess();
        }

        private void AddNumbersForNumberOfTimesToProcess()
        {
            var times = Enumerable.Range(1, 10000);

            foreach (var time in times)
            {
                dropdown_NumberOfTimesToProcess.Items.Add(time);
            }

            dropdown_NumberOfTimesToProcess.SelectedIndex = 0;
        }

        private void btn_TestIt_Click(object sender, EventArgs e)
        {
            int numTimesToProcess = int.Parse(dropdown_NumberOfTimesToProcess.SelectedItem.ToString());
            
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < numTimesToProcess; i++)
            {
                Process testApp = new Process();
                testApp.StartInfo.FileName = txtbx_PathToApplication.Text;
                testApp.Start();
                
                testApp.Kill();
            }
            sw.Stop();

            string timeToProcess;
            if ((sw.Elapsed.TotalMinutes <= 1) && (sw.Elapsed.TotalSeconds <= 1))
            {
                timeToProcess = String.Format("{0} Milliseconds", sw.Elapsed.TotalMilliseconds );
            }
            else if ((sw.Elapsed.TotalSeconds <= 10) && (sw.Elapsed.TotalMilliseconds >= 1))
            {
                timeToProcess = String.Format("{0} Seconds", sw.Elapsed.TotalSeconds);
            }
            else
            {
                timeToProcess = String.Format("{0} Minutes", sw.Elapsed.TotalMinutes);
            }

            lbl_TimeToProcess.Text = String.Format("{0}", timeToProcess);
        }
    }
}
